﻿using System;
using System.Collections.Generic;
using Carbon.Api.Data;

namespace Carbon.Api.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public ApplicationUser Author { get; set; }
        public List<PostTag> PostTags { get; set; }
    }
}