﻿namespace Carbon.Api.Models
{
    public enum AuthenticationLevel
    {
        Custom,
        User,
        Author,
        Editor,
        Admin
    }
}