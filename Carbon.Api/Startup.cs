﻿using System;
using Carbon.Api.Controllers;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Carbon.Api
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private IHostingEnvironment Env { get; }
        
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Sets up the connection to the postgressql db, Remeber to set this as an eviroment variable for your environement
            var sqlConnectionString = Environment.GetEnvironmentVariable("DATABASE_URL");
            
            services.AddDbContext<BlogContext>(opt => opt.UseNpgsql(sqlConnectionString));

            services.AddMvc();
                

            //require API to be https in staging and production
            if (!Env.IsDevelopment())
            {
                services.Configure<MvcOptions>(o => o.Filters.Add(new RequireHttpsAttribute()));
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}