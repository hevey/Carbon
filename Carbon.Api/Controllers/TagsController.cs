﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Authorization;

namespace Carbon.Api.Controllers
{
    [Route("/carbon/api/[controller]")]
    public class TagsController : Controller
    {
        private readonly BlogContext _context;

        public TagsController(BlogContext context)
        {
            _context = context;

            if (!_context.Tags.Any())
            {
                _context.Tags.Add(new Tag {PostTags = new List<PostTag>(), Value = "Test Tag"});
                _context.SaveChanges();
            }            
        }

        [HttpGet]
        public IEnumerable<Tag> GetAll()
        {
            return _context.Tags.ToList();
        }

        [HttpGet("{id}", Name = "GetTags")]
        public IActionResult GetById(int id)
        {
            var item = _context.Tags.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        
        [Authorize]
        [HttpPost]
        public IActionResult Create([FromBody] Tag tag)
        {
            if (tag == null)
            {
                return BadRequest();
            }
            _context.Tags.Add(tag);
            _context.SaveChanges();

            return CreatedAtRoute("GetTags", new {id = tag.Id}, tag);
        }
    }
}