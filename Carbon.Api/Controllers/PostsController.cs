﻿using System;
using System.Collections.Generic;
using System.Linq;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Carbon.Api.Controllers
{
    [Route("/carbon/api/[controller]")]
    public class PostsController : Controller
    {
        private readonly BlogContext _context;

        public PostsController(BlogContext context)
        {
            _context = context;
        } 
        
        [HttpGet]
        public IEnumerable<Post> GetAll()
        {
            return _context.Posts.ToList();
        }
        
        [HttpGet("{id}", Name = "GetPosts")]
        public IActionResult GetById(int id)
        {
            var item = _context.Posts.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create([FromBody] Post post)
        {
            if (post == null)
            {
                return BadRequest();
            }
            _context.Posts.Add(post);
            _context.SaveChanges();

            return CreatedAtRoute("GetPosts", new {id = post.Id}, post);
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Post post)
        {
            if (post == null || post.Id != id)
            {
                return BadRequest();
            }

            var returnedPost = _context.Posts.FirstOrDefault(p => p.Id == id);
            if (returnedPost == null)
            {
                return NotFound();
            }

            returnedPost.Author = post.Author;
            returnedPost.Content = post.Content;
            returnedPost.UpdatedTime = DateTime.Now;
            returnedPost.Title = post.Title;

            _context.Posts.Update(returnedPost);
            _context.SaveChanges();
            
            return new NoContentResult();
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var returnedPost = _context.Posts.FirstOrDefault(p => p.Id == id);
            if (returnedPost == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(returnedPost);
            _context.SaveChanges();
            
            return new NoContentResult();
        }
        
//        [Authorize]
//        [HttpPut("{id}/tags")]
//        public IActionResult SetTags(int id, [FromBody] List<int> tagList)
//        {
//            var tags = new List<Tag>();
//
//            foreach (var tagId in tagList)
//            {
//                var returnedTag = _context.Tags.FirstOrDefault(t => t.Id == tagId);
//                if(returnedTag == null)
//                {
//                    return NotFound();
//                }
//                
//                tags.Add(returnedTag);
//            }
//
//            if (tags.Count == 0)
//            {
//                return BadRequest();
//            }
//            
//            var post = _context.Posts.FirstOrDefault(p => p.Id == id);
//            if (post == null)
//            {
//                return NotFound();
//            }
//
//            foreach (var tag in tags)
//            {
//                var postTag = new PostTag { PostId = post.Id, TagId = tag.Id};              
//                _context.PostTags.Add(postTag);
//            }
//            
//            
//
//            _context.SaveChanges();
//            
//            return new NoContentResult();
//        }

        [HttpGet("{id}/tags")]
        public IActionResult GetTagsFromPost(int id)
        {
            var post = _context.Posts
                .Where(p => p.Id == id)
                .Include(p => p.PostTags)
                .ThenInclude(p => p.Tag)
                .FirstOrDefault();
            
            if (post == null)
            {
                return NotFound();
            }
            
            var tags = new List<string>();

            foreach (var postTag in post.PostTags)
            {
                tags.Add(postTag.Tag.Value);
            }
            
            return new ObjectResult(tags);
        }
    }
}