﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Carbon.Api.Data;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Carbon.AdminSite.Pages
{
    [Authorize]
    public class EditorModel : PageModel
    {
        [BindProperty]
        public Post Post { get; set; }

        [BindProperty]
        public bool Edited { get; set; } = false;

        private readonly BlogContext _blogContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;

        public EditorModel(BlogContext context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _blogContext = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        

        public async Task<IActionResult> OnGetEditorAsync(int id)
        {
            Post = await _blogContext.Posts
                .Include(P => P.PostTags)
                .SingleOrDefaultAsync(P => P.Id == id);
                


            if (Post == null)
            {
                return RedirectToPage("/Carbon/Index");
            }

            Edited = true;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            var user = await _userManager.GetUserAsync(User);
            Post.CreateTime = DateTime.Now;
            Post.Author = user;
            Post.PostTags = new List<PostTag>();
            
            var returnedTag = _blogContext.Tags.FirstOrDefault(t => t.Id == 2);

            Post.PostTags.Add(new PostTag
            {
                Post = Post,
                Tag = returnedTag
            });
            
            _blogContext.Posts.Add(Post);


            

            await _blogContext.SaveChangesAsync();
            return RedirectToPage("/Carbon/Index");
        }

        public async Task<IActionResult> OnPostEditAsync(int id)
        {
            var user = await _userManager.GetUserAsync(User);


            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            var returnedTag = _blogContext.Tags.FirstOrDefault(t => t.Id == 1);

            var postToUpdate = await _blogContext.Posts
                .Include(P => P.PostTags)
                .SingleAsync(P => P.Id == id);

            postToUpdate.UpdatedTime = DateTime.Now;
            postToUpdate.PostTags.Add(new PostTag { Post = postToUpdate, Tag = returnedTag });
            
            
            if (await TryUpdateModelAsync<Post>(
                postToUpdate,
                "post",
                p => p.UpdatedTime, p => p.Title, p => p.Content, p => p.PostTags))
            {
                
                await _blogContext.SaveChangesAsync();
                return RedirectToPage("/Carbon/Index");
            }


            return RedirectToPage("/Carbon/Index");
        }

        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            var post = await _blogContext.Posts.FindAsync(id);

            if (post != null)
            {
                _blogContext.Posts.Remove(post);
                await _blogContext.SaveChangesAsync();
            }

            return RedirectToPage("/Carbon/Index");
        }
    }
}