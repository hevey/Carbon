﻿using System.Collections.Generic;
using System.Linq;
using Carbon.Api.Controllers;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Carbon.AdminSite.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly BlogContext _blogContext;
        
        public List<Post> Posts;

        public IndexModel(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }
        public void OnGetAsync()
        {
            Posts = new PostsController(_blogContext).GetAll().ToList();
            
        }
    }
}