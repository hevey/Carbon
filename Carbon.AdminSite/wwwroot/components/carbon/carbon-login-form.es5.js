/*! Built with http://stenciljs.com */
carbon.loadComponents(function (exports, h, Context) {
    "use strict";
    Object.defineProperty(exports, '__esModule', { value: true });
    var CarbonLoginForm = /** @class */ (function () {
        function CarbonLoginForm() {
        }
        CarbonLoginForm.prototype.render = function () {
            return (h("slot", null));
        };
        Object.defineProperty(CarbonLoginForm, "is", {
            get: function () { return "carbon-login-form"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CarbonLoginForm, "properties", {
            get: function () { return { "Email": { "type": String }, "Password": { "type": String }, "RememberMe": { "type": String } }; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CarbonLoginForm, "style", {
            get: function () { return ""; },
            enumerable: true,
            configurable: true
        });
        return CarbonLoginForm;
    }());
    exports.CarbonLoginForm = CarbonLoginForm;
}, "carbon-login-form");
