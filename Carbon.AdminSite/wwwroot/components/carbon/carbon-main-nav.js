/*! Built with http://stenciljs.com */
const { h, Context } = window.carbon;

class CarbonMainNav {
    render() {
        return (h("ul", null,
            h("li", null,
                h("slot", { name: "logo" })),
            h("ul", { class: "right dropdown" },
                h("li", null,
                    h("h2", null, this.Name),
                    h("h3", null, this.Date),
                    h("ul", { class: "dropdown-content" },
                        h("li", null,
                            h("slot", { name: "logout" })))),
                h("li", null,
                    h("img", { id: "profile-image", src: this.ProfileImage })))));
    }
    static get is() { return "carbon-main-nav"; }
    static get properties() { return { "Date": { "type": String }, "Name": { "type": String }, "ProfileImage": { "type": String } }; }
    static get style() { return "carbon-main-nav {\n  overflow: hidden;\n  width: 100%;\n  /* Dropdown Content (Hidden by Default) */\n  /* Links inside the dropdown */\n  /* Change color of dropdown links on hover */\n  /* Show the dropdown menu on hover */\n}\n\ncarbon-main-nav ul {\n  margin: 0;\n  padding: 0;\n  list-style-type: none;\n}\n\ncarbon-main-nav li {\n  float: left;\n  position: relative;\n  height: 110px;\n  vertical-align: middle;\n  cursor: default;\n}\n\ncarbon-main-nav li h2 {\n  padding: 0;\n  margin: 0;\n  margin-top: 30px;\n  font-family: 'Muli', sans-serif;\n  font-weight: 600;\n  font-size: 1.4em;\n  color: #B1BDCF;\n}\n\ncarbon-main-nav li h3 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Muli', sans-serif;\n  font-weight: 400;\n  font-size: 0.8em;\n  color: #B1BDCF;\n}\n\ncarbon-main-nav li a {\n  display: block;\n  color: #B1BDCF;\n  text-align: center;\n  padding-left: 7px;\n  padding-right: 7px;\n  text-decoration: none;\n  padding-top: 30px;\n  font-family: 'Muli', sans-serif;\n  font-weight: 400;\n  font-size: 0.8em;\n}\n\ncarbon-main-nav li #profile-image {\n  width: 108px;\n  height: auto;\n}\n\ncarbon-main-nav li .logo-image {\n  width: 200px;\n  height: auto;\n}\n\ncarbon-main-nav .right {\n  float: right;\n  position: relative;\n}\n\ncarbon-main-nav .dropdown {\n  position: relative;\n  display: inline-block;\n}\n\ncarbon-main-nav .dropdown-content {\n  display: none;\n  position: absolute;\n  background-color: #f9f9f9;\n  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  z-index: 1;\n}\n\ncarbon-main-nav .dropdown-content a {\n  color: black;\n  padding: 12px 16px;\n  text-decoration: none;\n  display: block;\n}\n\ncarbon-main-nav .dropdown-content a:hover {\n  background-color: #f1f1f1;\n  cursor: pointer;\n}\n\ncarbon-main-nav .dropdown:hover .dropdown-content {\n  display: block;\n  cursor: pointer;\n}"; }
}

export { CarbonMainNav };
