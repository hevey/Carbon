/*! Built with http://stenciljs.com */
const { h, Context } = window.carbon;

class CarbonLoginForm {
    render() {
        return (h("slot", null));
    }
    static get is() { return "carbon-login-form"; }
    static get properties() { return { "Email": { "type": String }, "Password": { "type": String }, "RememberMe": { "type": String } }; }
    static get style() { return ""; }
}

export { CarbonLoginForm };
