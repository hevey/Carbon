﻿using System;
using System.Linq;
using Carbon.Api.Controllers;
using Carbon.Api.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;

namespace Carbon.AdminSite.Models
{
    [HtmlTargetElement("post-tags", Attributes = "post-id")]
    public class EditorPostTaggingTagHelper : TagHelper
    {

        [HtmlAttributeName("post-id")]
        public string PostId { get; set; }

        private BlogContext _blogContext;

        public EditorPostTaggingTagHelper(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }
        
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            output.TagMode = TagMode.StartTagAndEndTag;
            output.TagName = "post-tag";

            var tags = _blogContext.Posts
                .Where(p => p.Id == Int32.Parse(PostId))
                .Include(p => p.PostTags)
                .SelectMany(p => p.PostTags)
                .Select(p => p.Tag);

            foreach (var tag in tags)
            {
                output.Content.Append(tag.Value + " ");
            }

           
            
            
            
        }
    }
}